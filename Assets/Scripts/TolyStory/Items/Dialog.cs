﻿using System;
using TolyStory.UI;

namespace TolyStory.Items
{
    [Serializable]
    public class Dialog
    {
        public Res.CharacterName name;
        public string message;
    }
}
