﻿using System;
using UnityEngine;

namespace TolyStory.Items
{
    [Serializable]
    public class DialogSequence : MonoBehaviour
    {
        public Dialog[] dialogArray;
        public Dialog defaultDialog;

        private int index = 0;

        public Dialog Next()
        {
            if (index >= dialogArray.Length)
            {
                return defaultDialog;
            }
            else
            {
                return dialogArray[index++];
            }
        }

        public bool IsLast()
        {
            return index >= dialogArray.Length;
        }
    }
}
