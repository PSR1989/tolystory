﻿using TolyStory.Controllers;
using TolyStory.Engine;
using UnityEngine;

namespace TolyStory.Items
{
    public class DialogTrigger : MonoBehaviour
    {
        public DialogController dialogController;

        private bool inTrigger = false;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //GameEngine.instance.inDialogTrigger = true;
            //GameEngine.instance.currentDialogSequnce = dialogController.GetDialog();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            //GameEngine.instance.inDialogTrigger = true;
            //GameEngine.instance.currentDialogSequnce = dialogController.GetDialog();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            //GameEngine.instance.inDialogTrigger = false;

            // reset game condition
            //GameEngine.instance.currentActionKey = EngineKeys.ActionKeys.NONE;
        }

    }
}
