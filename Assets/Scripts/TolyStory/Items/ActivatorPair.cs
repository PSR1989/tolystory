﻿using System;
using UnityEngine;

namespace TolyStory.Items
{
    [Serializable]
    public class ActivatorPair
    {
        public GameObject gameObject;
        public bool activate;
    }
}
