﻿using UnityEngine;
using TolyStory.Engine;
using System.Collections;
using TolyStory.Res;

namespace TolyStory.Items
{
    public class DoorTrigger : MonoBehaviour
    {
        public MyScene parentScene;
        public DoorTrigger nextTrigger;
        public DoorSound doorSound = DoorSound.Simple;

        [Tooltip("true = right")]
        public bool playerDirection;
        private bool inTrigger = false;
        private bool isActivate = false;

        private void Update()
        {
            if (inTrigger && GameEngine.instance.input.special && !isActivate)
            {
                isActivate = true;
                GameEngine.instance.StartNewState(Values.STATE_TRANSITION);

                // play sound
                GameEngine.instance.audioManager.PlayDoorSound(doorSound);
                // add listener
                GameEngine.instance.gui.uiFader.OnFadeIn.AddListener(OnFaderFadeIn);
            }
        }
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            UpdateGameEngine();
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            UpdateGameEngine();
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            GameEngine.instance.gui.HideSpecialBtn();
            inTrigger = false;
        }

        private void UpdateGameEngine()
        {
            GameEngine.instance.gui.ShowSpecialBtn(Res.Values.GetGUIString(Res.GUIActionButtonName.Open));
            inTrigger = true;
            GameEngine.instance.player.SetNextXPosAndDirection(nextTrigger.gameObject.transform.position.x, nextTrigger.playerDirection);
        }

        private void OnFaderFadeIn()
        {
            parentScene.Deactivate();
            nextTrigger.parentScene.Activate();

            
            // remove listener
            GameEngine.instance.gui.uiFader.OnFadeIn.RemoveListener(OnFaderFadeIn);

        }

        private void OnEnable()
        {
            // reset
            isActivate = false;
        }
    }
}
