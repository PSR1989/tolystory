﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace TolyStory.Items
{
    public class MyScene : MonoBehaviour
    {

        private Sprite backgroundImage;
        private float maxX;

        private void Start()
        {
            backgroundImage = GetComponent<SpriteRenderer>().sprite;

            // calculate max right X axis coordinate for camera movement limit
            maxX = backgroundImage.rect.width/backgroundImage.pixelsPerUnit;

            GameEngine.instance.cameraControler.SetMaxX(maxX);
        }
        public void Activate()
        {
            gameObject.SetActive(true);
            GameEngine.instance.cameraControler.SetMaxX(maxX);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}
