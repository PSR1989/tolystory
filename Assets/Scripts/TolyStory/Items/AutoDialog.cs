﻿using TolyStory.EngineAction;
using UnityEngine;

namespace TolyStory.Items
{
    public class AutoDialog : MonoBehaviour
    {
        public EAction action;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameEngine.instance.curAction = action;
            GameEngine.instance.RunCallback();
            Destroy(gameObject);
        }
    }
}
