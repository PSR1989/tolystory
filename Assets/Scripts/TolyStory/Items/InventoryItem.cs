﻿using TolyStory.Res;
using UnityEngine;

namespace TolyStory.Items
{
    public class InventoryItem : MonoBehaviour
    {
        public Sprite icon;
        public InventoryItemName itemName;
    }
}
