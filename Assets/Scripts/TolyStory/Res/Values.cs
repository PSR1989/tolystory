﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TolyStory.Res
{
    public static class Values
    {
        public static readonly string CHARACTER_CHEGEZE  = "чегезе";
        public static readonly string CHARACTER_VLADIMIR = "владимир";
        public static readonly string CHARACTER_ALEXANDER = "александр";
        public static readonly string CHARACTER_DOCS = "людмила";
        public static readonly string CHARACTER_CHEF = "шеф";
        public static readonly string CHARACTER_HERO = "анатоль";

        // states
        public static readonly int STATE_START = 0;
        public static readonly int STATE_DIALOG = 1;
        public static readonly int STATE_COMMON = 2;
        public static readonly int STATE_TRANSITION = 3;
        public static readonly int STATE_JOURNAL = 4;
        public static readonly int STATE_FINAL = 5;

        // gui
        public static readonly string GUI_ACTION_JOURNAL = "Инвентарь";
        public static readonly string GUI_ACTION_NEXT = "Дальше";
        public static readonly string GUI_ACTION_CLOSE = "Выйти";
        public static readonly string GUI_ACTION_OPEN = "Открыть";
        public static readonly string GUI_ACTION_TALK = "Поговорить";
        public static readonly string GUI_ACTION_ACTION = "Взаимодействовать";
        public static readonly string GUI_NOTIFY_DEFAULT = "Инвентарь обновился";
        public static readonly string GUI_ACTION_FIX = "Починить";
        public static readonly string GUI_ACTION_WATCH = "Осмотреть";

        public static string GetGUIString(GUIActionButtonName key)
        {
            switch (key)
            {
                case GUIActionButtonName.Inventory:
                    return GUI_ACTION_JOURNAL;
                case GUIActionButtonName.Next:
                    return GUI_ACTION_NEXT;
                case GUIActionButtonName.Close:
                    return GUI_ACTION_CLOSE;
                case GUIActionButtonName.Open:
                    return GUI_ACTION_OPEN;
                case GUIActionButtonName.Talk:
                    return GUI_ACTION_TALK;
                case GUIActionButtonName.Action:
                    return GUI_ACTION_ACTION;
                case GUIActionButtonName.Fix:
                    return GUI_ACTION_FIX;
                case GUIActionButtonName.Watch:
                    return GUI_ACTION_WATCH;
                default:
                    return "";
            }
        }
    }

    public enum DoorSound
    {
        Simple,
        Station,
        Car,
        WithOut
    }

    public enum NPCSound
    {
        a,
        b,
        c,
        d
    }

    public enum GUIActionButtonName
    {
        Inventory,
        Next,
        Close,
        Open,
        Talk,
        Action,
        Fix,
        Watch
    }

    public enum CharacterName
    {
        Chegeze = 0,
        Vladimir = 1,
        Alexander = 2,
        Docs = 3,
        Chef = 4,
        Hero= 5,
        Phone = 6,
        Info = 7,
        Operator = 8,
        Abonent = 9
    }

    public enum InventoryItemName
    {
        Perf = 0,
        Basic = 1,
        Docs = 2,
        Dsl = 3,
        Socket = 4
    }
}
