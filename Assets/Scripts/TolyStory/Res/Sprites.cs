﻿using UnityEngine;

namespace TolyStory.Res
{
    public class Sprites : MonoBehaviour
    {
        public Sprite basicSprite;
        public Sprite dslSprite;
        public Sprite docsSprite;
        public Sprite perfSprite;
        public Sprite socketSprite;
        public Sprite gateKeySprite;
        public Sprite stationKeySprite;
        public Sprite wireSprite;
    }
}
