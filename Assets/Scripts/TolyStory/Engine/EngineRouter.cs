﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace TolyStory.Engine
{
    public class EngineRouter : MonoBehaviour
    {

        public EngineController[] controllers;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            foreach( EngineController c in controllers)
            {
                c.SetEngineValues();
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            foreach (EngineController c in controllers)
            {
                c.UnsetEngineValues();
            }
        }


    }
}
