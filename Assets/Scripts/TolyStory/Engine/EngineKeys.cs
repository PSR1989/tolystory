﻿namespace TolyStory.Engine
{
    public static class EngineKeys
    {
        public enum ConditionKeys {
            NONE,
            ALWAYS_TRUE,
            ALWAYS_FALSE,
            // scene 0
            PLAYER_HAS_BASICS,
            PLAYER_HAS_DSL,
            PLAYER_HAS_DOCS,
            PLAYER_HAS_PERF,
            PLAYER_HAS_TELEPHONESOCKET,
            PLAYER_HAS_INSTRUMENT,      // когда собраны все инструменты, устанавливается автоматом

            // scene 1
            SCENE1_QUEST_POST_COMPLETE,
            PLAYER_HAS_CABLE,
            PLAYER_HAS_GATE_KEYS,
            PLAYER_HAS_STATION_KEY,
            SCENE1_QUEST_CROSS_COMPLETE,


            // scene 2
            SCENE2_TALK_WITH_ABONENT,
            SCENE2_INSIDE_SOCKET_READY,
            SCENE2_INTERNET_READY,
            SCENE2_DSL_READY,
            SCENE2_SHR_CHECK,
            SCENE2_UKS_CHECK
        };
    }
}
