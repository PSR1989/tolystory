﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace TolyStory.Engine
{
    public class LoadGame : MonoBehaviour
    {

        private void Start()
        {
            SceneManager.LoadSceneAsync(1);
        }
    }
}
