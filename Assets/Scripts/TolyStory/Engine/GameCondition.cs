﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TolyStory.Engine
{
    public class GameCondition
    {
        private Dictionary<EngineKeys.ConditionKeys, bool> values;

        public GameCondition()
        {
            values = new Dictionary<EngineKeys.ConditionKeys, bool>
            {
                {EngineKeys.ConditionKeys.ALWAYS_TRUE, true },
                {EngineKeys.ConditionKeys.ALWAYS_FALSE, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_BASICS, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_DSL, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_DOCS, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_PERF, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_TELEPHONESOCKET, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_INSTRUMENT, false }, // scene 0 когда собран весь инструмент
                {EngineKeys.ConditionKeys.SCENE1_QUEST_POST_COMPLETE, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_CABLE, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_GATE_KEYS, false },
                {EngineKeys.ConditionKeys.PLAYER_HAS_STATION_KEY, false },
                {EngineKeys.ConditionKeys.SCENE1_QUEST_CROSS_COMPLETE, false },
                {EngineKeys.ConditionKeys.SCENE2_TALK_WITH_ABONENT, false },
                {EngineKeys.ConditionKeys.SCENE2_INSIDE_SOCKET_READY, false },
                {EngineKeys.ConditionKeys.SCENE2_INTERNET_READY, false },
                { EngineKeys.ConditionKeys.SCENE2_SHR_CHECK, false},
                {EngineKeys.ConditionKeys.SCENE2_UKS_CHECK, false },
                {EngineKeys.ConditionKeys.SCENE2_DSL_READY, false }
            };
        }

        public bool GetValue(EngineKeys.ConditionKeys key)
        {
            return values[key];
        }

        public void SetValue(EngineKeys.ConditionKeys key, bool value)
        {
            values[key] = value;
        }
    }
}
