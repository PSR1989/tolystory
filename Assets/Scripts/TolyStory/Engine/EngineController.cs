﻿using System;

namespace TolyStory.Engine
{
    [Serializable]
    public class EngineController
    {
        public virtual void SetEngineValues() { }
        public virtual void UnsetEngineValues() { }
    }
}
