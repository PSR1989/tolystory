﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace TolyStory.UI
{
    public class UIFader : MonoBehaviour
    {
        public UnityEvent OnFadeIn;
        public UnityEvent OnFadeOut;

        private Image fadeImage;

        private float curOpacity = 0;
        private Color curColor = new Color(0, 0, 0, 0);
        private bool upState = true;

        private void Start()
        {
            fadeImage = GetComponent<Image>();
            gameObject.SetActive(false);
        }
        private void Update()
        {
            var delta = Time.deltaTime;

            if (upState)
            {
                curOpacity += 2 * delta;

                if (curOpacity >= 1)
                {
                    curOpacity = 1.5f;

                    OnFadeIn?.Invoke();
                    upState = false;
                }
            }
            else
            {
                curOpacity -= 2 * delta;

                if (curOpacity < 0)
                {
                    // reset
                    upState = true;
                    curOpacity = 0;

                    // apply
                    curColor.a = curOpacity;
                    fadeImage.color = curColor;

                    // invoke
                    OnFadeOut?.Invoke();

                    gameObject.SetActive(false);
                    return;
                }
            }

            curColor.a = curOpacity;
            fadeImage.color = curColor;
        }
        public void Fade()
        {
            gameObject.SetActive(true);
        }
    }
}
