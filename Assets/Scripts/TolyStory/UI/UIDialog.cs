﻿using TolyStory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace TolyStory.UI
{
    public class UIDialog : MonoBehaviour
    {
        public string characterName;
        public Text messageText;
        public Text characterText;
    }
}
