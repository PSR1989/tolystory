﻿using TolyStory.Items;
using TolyStory.Res;
using UnityEngine;

namespace TolyStory.UI
{
    public class UIDialogSystem: MonoBehaviour
    {
        public UIDialog[] uiDialogs;

        private int currentIndex = 0;

        // show root dialog container
        public void StartDialog()
        {
            gameObject.SetActive(true);
        }

        // hide root dialog container
        public void StopDialog()
        {
            gameObject.SetActive(false);
        }

        // show dialog
        public void Show(Dialog dialog)
        {
            HidePrevDialog();

            currentIndex = (int)dialog.name;
            var tempUIDialog = uiDialogs[currentIndex];

            tempUIDialog.characterText.text = tempUIDialog.characterName;
            tempUIDialog.messageText.text = dialog.message;
            tempUIDialog.gameObject.SetActive(true);
        }

        public void HidePrevDialog()
        {
            uiDialogs[currentIndex].gameObject.SetActive(false);
        }
    }
}
