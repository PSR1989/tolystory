﻿using TolyStory.Engine;
using UnityEngine;
using UnityEngine.UI;

namespace TolyStory.UI
{
    public class UIInventory : MonoBehaviour
    {
        [SerializeField] Image[] items;

        private GameCondition gC;

        public void _Update()
        {
            gC = GameEngine.instance.gameCondition;

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_BASICS))
            {
                items[0].sprite = GameEngine.instance.sprites.basicSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_DSL))
            {
                items[1].sprite = GameEngine.instance.sprites.dslSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_PERF))
            {
                items[2].sprite = GameEngine.instance.sprites.perfSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_TELEPHONESOCKET))
            {
                items[3].sprite = GameEngine.instance.sprites.socketSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_DOCS))
            {
                items[4].sprite = GameEngine.instance.sprites.docsSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_CABLE))
            {
                items[5].sprite = GameEngine.instance.sprites.wireSprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_GATE_KEYS))
            {
                items[6].sprite = GameEngine.instance.sprites.gateKeySprite;
            }

            if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_STATION_KEY))
            {
                items[7].sprite = GameEngine.instance.sprites.stationKeySprite;
            }

            //if (gC.GetValue(EngineKeys.ConditionKeys.PLAYER_HAS_BASICS))
            //{
            //    items[0].sprite = GameEngine.instance.sprites.basicSprite;
            //}
        }
    }
}
