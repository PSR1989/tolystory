﻿using UnityEngine;

namespace TolyStory.UI
{
    public class UpdateInputUI : MonoBehaviour
    {
        [Header("true=dialog")]
        public bool isDialogOrDoor;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (isDialogOrDoor)
            {
                GameEngine.instance.gui.SetActionButtonNameToStartDialog();
            }
            else
            {
                GameEngine.instance.gui.SetActionButtonNameToOpenDoor();
            }
            
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            GameEngine.instance.gui.SetActionButtonNameToOpenJournal();
        }
    }
}
