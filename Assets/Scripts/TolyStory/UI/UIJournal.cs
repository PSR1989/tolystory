﻿using UnityEngine;

namespace TolyStory.UI
{
    public class UIJournal : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
