﻿using UnityEngine;
using UnityEngine.UI;

namespace TolyStory.UI
{
    public class UINotificator : MonoBehaviour
    {
        [SerializeField] Image backgroundImage;
        [SerializeField] Text message;

        public float showTime;

        private float timer;

        private Color backgroundColor = new Color(0, 0, 0, 1);
        private Color messageColor = new Color(1, 1, 1, 1);
        private float currentOpacity = 1;

        private void Start()
        {
            timer = 0;
            currentOpacity = 1;
        }
        private void Update()
        {
            timer += Time.deltaTime;

            if (timer > showTime)
            {
                // fade out
                currentOpacity = 1 - (timer - showTime);

                if (currentOpacity <= 0)
                {
                    currentOpacity = 0;
                    ApplyValues();
                    gameObject.SetActive(false); // self inactivate
                    return;
                }
                else
                {
                    ApplyValues();
                }
            }
        }
        private void OnEnable()
        {
            // apply reset values
            ApplyValues();
        }
        private void OnDisable()
        {
            //reset
            timer = 0;
            currentOpacity = 1;
        }
        private void ApplyValues()
        {
            backgroundColor.a = currentOpacity;
            messageColor.a = currentOpacity;

            backgroundImage.color = backgroundColor;
            message.color = messageColor;
        }
        
        public void Notify()
        {
            message.text = Res.Values.GUI_NOTIFY_DEFAULT;
            gameObject.SetActive(true);
        }

        public void Notify(string message)
        {
            this.message.text = message;
            gameObject.SetActive(true);
        }
    }
}
