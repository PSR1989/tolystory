﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TolyStory.Controllers
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] Transform target;

        private float horzExtent;

        private float maxX;

        public float smoothingRatio = 5;

        private float leftBound;
        private float rightBound;

        // custom variables
        Vector3 offset;
        Transform m_Transform;
        // Start is called before the first frame update
        void Start()
        {
            m_Transform = GetComponent<Transform>();
            offset = m_Transform.position - target.position;

            horzExtent = Camera.main.orthographicSize * Screen.width / Screen.height;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            Vector3 nextPos = offset + target.position;
            
            leftBound = nextPos.x - horzExtent;
            rightBound = nextPos.x + horzExtent;

            if (leftBound <= 0)
            {
                nextPos.x = 0 + horzExtent;
            }

            if (rightBound >= maxX)
            {
                nextPos.x = maxX - horzExtent;
            }

            m_Transform.position = Vector3.Slerp(m_Transform.position, nextPos, Time.deltaTime * smoothingRatio);
        }

        public void SetMaxX(float x)
        {
            maxX = x;
        }
    }
}

