﻿using TolyStory.Engine;
using TolyStory.EngineAction;
using UnityEngine;

namespace TolyStory.Controllers
{
    public class LogicController : MonoBehaviour
    {
        public EngineKeys.ConditionKeys condition;
        public Res.GUIActionButtonName btnName = Res.GUIActionButtonName.Action;

        [Header("Действия. IF TRUE => actionA")]
        public EAction actionA;
        public EAction actionB;

        private bool inTrigger = false;

        private void Update()
        {
            if (GameEngine.instance.input.special && inTrigger)
            {

                GameEngine.instance.curAction = GameEngine.instance.gameCondition.GetValue(condition) ? actionA : actionB;
                GameEngine.instance.RunCallback();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            SetupEngine();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            SetupEngine();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            // reset 
            GameEngine.instance.curAction = null;
            GameEngine.instance.gui.HideSpecialBtn();
            inTrigger = false;
        }

        private void SetupEngine()
        {
            if (!inTrigger)
            {
                GameEngine.instance.gui.ShowSpecialBtn(Res.Values.GetGUIString(btnName));
                inTrigger = true;
            }
        }


    }
}
