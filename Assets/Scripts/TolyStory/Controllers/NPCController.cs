﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TolyStory.Items;
using TolyStory.Engine;
using TolyStory.EngineAction;
using TolyStory.Qu;
using TolyStory.Res;

namespace TolyStory.Controllers
{
    public class NPCController : MonoBehaviour
    {
        [Header("Dialogs")]
        public DialogSequence dialogA;
        public DialogSequence dialogB;
        public bool noDialogB;
        public NPCSound soundType = NPCSound.a;

        [Header("Conditions and post action")]
        [Tooltip("Если условие выполнено, то установить для NPC диалог 'B' и колбек")]
        public EngineKeys.ConditionKeys condition;
        public EAction postDialogAAction;
        public EAction postDialogBAction;

        public Res.GUIActionButtonName btnName = Res.GUIActionButtonName.Talk;

        private bool inTrigger = false;
        private DialogSequence currentDialogSequnce;

        private void Update()
        {
            if (GameEngine.instance.input.special && inTrigger)
            {
                // play sound
                GameEngine.instance.audioManager.PlayNPCSOund(soundType);

                GameEngine.instance.StartDialogState(currentDialogSequnce);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameEngine game = GameEngine.instance;
            //game.inDialogTrigger = true;

            if (!inTrigger)
            {
                inTrigger = true;
                GameEngine.instance.gui.ShowSpecialBtn(Res.Values.GetGUIString(btnName));
            }

            if (noDialogB)
            {
                currentDialogSequnce = dialogA;
                game.curAction = postDialogAAction;
            }
            else
            {
                if (game.gameCondition.GetValue(condition))
                {
                    game.curAction = postDialogBAction;
                    currentDialogSequnce = dialogB;
                }
                else
                {
                    currentDialogSequnce = dialogA;
                    game.curAction = postDialogAAction;
                }
            }

        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            GameEngine game = GameEngine.instance;
            //game.inDialogTrigger = true;

            if(!inTrigger)
            {
                inTrigger = true;
                GameEngine.instance.gui.ShowSpecialBtn(Res.Values.GetGUIString(btnName));
            }

            if (noDialogB)
            {
                currentDialogSequnce = dialogA;
                game.curAction = postDialogAAction;
            }
            else
            {
                if (game.gameCondition.GetValue(condition))
                {
                    game.curAction = postDialogBAction;
                    currentDialogSequnce = dialogB;
                }
                else
                {
                    currentDialogSequnce = dialogA;
                    game.curAction = postDialogAAction;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            // reset
            GameEngine.instance.curAction = null;
            inTrigger = false;
            GameEngine.instance.gui.HideSpecialBtn();
        }
    }
}
