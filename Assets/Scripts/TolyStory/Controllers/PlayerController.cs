﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TolyStory.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [Header("main variables")]
        public float speed;

        [SerializeField] SpriteRenderer m_SpriteRenderer;
        [SerializeField] Animator spriteAnimator;
        private AudioSource m_AudioSource;

        [SerializeField] AudioClip stepA;
        [SerializeField] AudioClip stepB;

        Rigidbody2D m_Rigidbody2D;
        int curPosIndex = 0;
        
        // custom variables
        float moveX;
        bool facingRight = true;
        public bool canMove;

        private float nextXPos;
        private bool nextDirection;

        private string stepKeyA = "Sprite_Atlas_00_59";
        private string stepKeyB = "Sprite_Atlas_00_65";

        // Start is called before the first frame update
        void Start()
        {
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
            m_AudioSource = GetComponent<AudioSource>();
        }

        // Update is called once per frame
        public void _Update (float move, float delta)
        {
            if (canMove)
            {
                moveX = move * delta * speed;

                if (m_SpriteRenderer.sprite.name.Equals(stepKeyA) && moveX != 0)
                {
                    if (!m_AudioSource.isPlaying)
                    {
                        m_AudioSource.PlayOneShot(stepA);
                    }
                }
                else if (m_SpriteRenderer.sprite.name.Equals(stepKeyB) && moveX != 0)
                {
                    if (!m_AudioSource.isPlaying)
                    {
                        m_AudioSource.PlayOneShot(stepB);
                    }
                }

                spriteAnimator.SetBool("isWalking", Mathf.Abs(moveX) > 0.01f);
                Flip();
            }
        }

        private void FixedUpdate()
        {
            if (canMove)
            {
                m_Rigidbody2D.MovePosition(m_Rigidbody2D.position + Vector2.right * moveX);
            }
        }

        public void StopWalkingAnimation()
        {
            spriteAnimator.SetBool("isWalking", false);
        }

        private void Flip()
        {
            if (facingRight && moveX < -0.01)
            {
                facingRight = !facingRight;
                m_SpriteRenderer.flipX = !facingRight;
            }
            else if (!facingRight && moveX > 0.01)
            {
                facingRight = !facingRight;
                m_SpriteRenderer.flipX = !facingRight;
            }
        }

        // playerDirection = true => right
        public void MoveToPoint(Vector3 point, bool playerDirection)
        {
            m_Rigidbody2D.isKinematic = true;
            m_Rigidbody2D.position = point;
            if (facingRight != playerDirection)
            {
                moveX = 0;
                FlipTo(playerDirection);
            }
            m_Rigidbody2D.isKinematic = false;
        }

        public void MoveToNextXPos()
        {
            m_Rigidbody2D.isKinematic = true;
            m_Rigidbody2D.position = Vector2.right * nextXPos;
            if (facingRight != nextDirection)
            {
                moveX = 0;
                FlipTo(nextDirection);
            }
            m_Rigidbody2D.isKinematic = false;
        }

        public void SetNextXPosAndDirection(float x, bool nextDir)
        {
            nextXPos = x;
            nextDirection = nextDir;
        }

        private void FlipTo(bool dir)
        {
            facingRight = dir;
            m_SpriteRenderer.flipX = !facingRight;
        }
    }
}

