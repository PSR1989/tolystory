﻿using UnityEngine;

namespace TolyStory.States
{
    public class FinalState : State
    {

        float timer;

        bool canShowThaks = true;
        bool canShowCode = true;
        bool canShowExitBtn = true;
        public FinalState(GameEngine game) : base(game) { }

        public override void PreUpdate()
        {
            game.gui.ShowFinalBg();
            game.gui.HideInput();
        }

        public override void Update(float delta)
        {
            timer += delta;

            if (timer >= 8 && canShowThaks)
            {
                canShowThaks = false;
                game.gui.SetFinalText("Спасибо за игру");
            }
            else if (timer >= 12 && canShowCode)
            {
                canShowCode = false;
                game.gui.SetFinalText("Код на пиццу: x123");
            }
            else if (timer > 18 && canShowExitBtn)
            {
                canShowExitBtn = false;
                game.gui.ShowFinalBtn();
            }
        }
    }
}
