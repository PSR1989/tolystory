﻿using TolyStory.Res;
using TolyStory.Engine;

namespace TolyStory.States
{
    public class StartGame : State
    {
        public StartGame(GameEngine game) : base(game) { }
        
        public override void Update(float deltra)
        {
            //game.currentActionKey = EngineKeys.ActionKeys.SHOW_NOTIFICATION;
            game.StartNewState(Values.STATE_DIALOG);
        }
    }
}
