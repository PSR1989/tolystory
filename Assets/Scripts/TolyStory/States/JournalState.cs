﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TolyStory.Res;

namespace TolyStory.States
{
    public class JournalState : State
    {

        public JournalState(GameEngine game) : base(game) { }

        public override void PreUpdate()
        {
            game.gui.ShowJournal();
        }

        public override void Update(float delta)
        {
            if (!game.gui.isJournalShown)
            {
                game.StartNewState(Values.STATE_COMMON);
            }
        }

        public override void PostUpdate()
        {
            base.PostUpdate();
            // play close sound
        }
    }
}
