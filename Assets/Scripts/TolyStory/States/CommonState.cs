﻿using TolyStory.Controllers;
using TolyStory.Managers;
using TolyStory.Res;

namespace TolyStory.States
{
    public class CommonState : State
    {
        private PlayerController player;
        public CommonState(GameEngine game, PlayerController player) : base(game)
        {
            this.player = player;
        }

        public override void PreUpdate()
        {
            player.canMove = true;


        }

        public override void Update(float delta)
        {
            // read input
            if (game.input.action)
            {
                // play sound
                game.audioManager.PlayClick();

                game.StartNewState(Values.STATE_JOURNAL);
            }

            if (game.gameOver)
            {
                game.StartFinalState();
            }

            // update player
            player._Update(game.input.dx, delta);
        }

        public override void UpdateGUI()
        {
            game.gui.SetActionButtonNameToOpenJournal();
        }
    }
}
