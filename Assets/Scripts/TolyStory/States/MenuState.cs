﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TolyStory.States
{
    public class MenuState : State
    {

        public MenuState(GameEngine game) : base(game) { }

        public override void PreUpdate()
        {
            base.PreUpdate();
        }

        public override void Update(float delta)
        {
            base.Update(delta);
        }
    }
}
