﻿using UnityEngine;

namespace TolyStory.States
{
    public class Transition : State
    {
        public Transition(GameEngine game) : base(game) {}

        public override void PreUpdate()
        {
            game.gui.FadeEffect(); // exit from event handler
        }
    }
}
