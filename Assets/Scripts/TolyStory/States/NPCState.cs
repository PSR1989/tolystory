﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TolyStory.States
{
    public class NPCState : State
    {
        public NPCState(GameEngine game) : base(game) { }

        public override void PreUpdate()
        {

        }

        public override void Update(float delta)
        {
           
        }

        public override void PostUpdate()
        {
            
        }
    }
}
