﻿using System;
using TolyStory.EngineAction;
using TolyStory.Items;


namespace TolyStory.States
{
    [Serializable]
    public class DialogState : State
    {
        private DialogSequence currentDialogSequence;
        private EAction action;

        public DialogState(GameEngine game) : base(game) {}

        public override void PreUpdate()
        {
            // show dialog
            game.gui.StartDialog();
            game.gui.ShowDialog(currentDialogSequence.Next());

            // stop player
            game.player.canMove = false;
            game.player.StopWalkingAnimation();

            // udpate gui
            if (currentDialogSequence.IsLast())
            {
                // update UI
                game.gui.SetActionButtonNameToCloseDialog();
            }
            else
            {
                game.gui.SetActionButtonNameToNextDialog();
            }
        }

        public override void Update(float delta)
        {

            if (game.input.action)
            {
                // play sound
                game.audioManager.PlayClick();

                // колхозный код
                if (currentDialogSequence.IsLast())
                {
                    if (game.gameOver)
                    {
                        game.StartNewState(Res.Values.STATE_FINAL);
                    }
                    else
                    {
                        game.StartNewState(Res.Values.STATE_COMMON);
                    }
                    return;
                }

                // show dialog
                Dialog temp = currentDialogSequence.Next();
                game.gui.ShowDialog(temp);

                // update gui
                if (currentDialogSequence.IsLast())
                {
                    game.gui.SetActionButtonNameToCloseDialog();
                }
            }
        }

        public override void PostUpdate()
        {
            // close dialog
            game.gui.StopDialog();

            game.RunCallback();            
        }

        public void SetDialogSequence(DialogSequence ds)
        {
            currentDialogSequence = ds;
        }
    }
}
