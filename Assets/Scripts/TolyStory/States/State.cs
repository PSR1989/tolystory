﻿using System;

namespace TolyStory.States
{
    public class State
    {
        public GameEngine game;

        public State(GameEngine game)
        {
            this.game = game;
        }

        public virtual void PreUpdate() { }
        public virtual void Update(float delta) { }
        public virtual void PostUpdate() { }
        public virtual void UpdateGUI() { }
    }
}
