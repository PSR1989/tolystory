﻿namespace TolyStory.EngineAction
{
    class ShowNotification : EAction
    {
        public string message;

        public override void doAction()
        {
            GameEngine.instance.gui.ShowNotificator(message);
            GameEngine.instance.audioManager.PlayNotification();
        }
    }
}
