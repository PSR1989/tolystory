﻿using TolyStory.Items;
using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_SCENE2_UKS_CHECK : EAction
    {
        public DialogSequence dialog;
        public override void doAction()
        {
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.SCENE2_UKS_CHECK, true);
            GameEngine.instance.StartDialogState(dialog);
            GameEngine.instance.audioManager.PlayNPCSOund(Res.NPCSound.a);
        }
    }
}
