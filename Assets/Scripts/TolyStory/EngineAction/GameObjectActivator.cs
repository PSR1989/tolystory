﻿using TolyStory.Items;

namespace TolyStory.EngineAction
{
    public class GameObjectActivator : EAction
    {
        public Items.ActivatorPair[] pairs;
        public string postActionNotificationMessage;
        private bool canRun = true;

        public override void doAction()
        {
            if (canRun)
            {
                canRun = false;
                foreach (ActivatorPair pair in pairs)
                {
                    pair.gameObject.SetActive(pair.activate);
                }

                if (!string.IsNullOrEmpty(postActionNotificationMessage.Trim()))
                {
                    GameEngine.instance.audioManager.PlayNotification();
                    GameEngine.instance.gui.ShowNotificator(postActionNotificationMessage);
                }
            }
            else
            {
                return;
            }

        }
    }
}
