﻿using TolyStory.Items;
using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_HAS_STATION_KEY : EAction
    {
        public GameObject doorTrigger;
        public DialogSequence dialog;
        private bool canRun = true;

        public override void doAction()
        {
            if (!canRun)
            {
                return;
            }
            canRun = false;
            doorTrigger.SetActive(true);
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.PLAYER_HAS_STATION_KEY, true);
            GameEngine.instance.StartDialogState(dialog);
            GameEngine.instance.audioManager.PlayNPCSOund(Res.NPCSound.c);
            Destroy(gameObject);
        }
    }
}
