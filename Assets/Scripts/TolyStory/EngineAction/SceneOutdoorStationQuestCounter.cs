﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    /*
     * Квест на Почте. Оператор почты даёт задание починить телефон.
     * ГГ должен обойти 3 мусорки, что бы найти кабель.
     * 
     */

    class SceneOutdoorStationQuestCounter : EAction
    {
        public GameObject guiList;
        public GameObject findKeyObject;
        public override void doAction()
        {
            GameEngine.instance.sceneStationOutdorCounter++;

            if (GameEngine.instance.sceneStationOutdorCounter >= 3)
            {
                guiList.SetActive(true);
                findKeyObject.SetActive(true);
                GameEngine.instance.audioManager.PlayNotification();
            }
            else
            {
                GameEngine.instance.audioManager.PlayUrna();
                GameEngine.instance.gui.ShowNotificator("Под этим кирпичём нет ключа.");
            }

            Destroy(gameObject);
        }
    }
}
