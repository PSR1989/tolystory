﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class EAction : MonoBehaviour
    {
        public virtual void doAction() {}
        public virtual void doAction(string msg) {}
    }

}
