﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TolyStory.Items;
using TolyStory.Res;

namespace TolyStory.EngineAction
{
    public class ShowDialog : EAction
    {
        public DialogSequence dialog;
        public NPCSound npcSoundType;

        public override void doAction()
        {
            // Play sound
            GameEngine.instance.audioManager.PlayNPCSOund(npcSoundType);
            GameEngine.instance.StartDialogState(dialog);
        }
    }
}
