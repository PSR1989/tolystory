﻿using static TolyStory.Engine.EngineKeys;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_HAS_BASICS : EAction
    {
        public override void doAction()
        {
            var gameC = GameEngine.instance.gameCondition;
            if (!gameC.GetValue(ConditionKeys.PLAYER_HAS_BASICS))
            {
                gameC.SetValue(ConditionKeys.PLAYER_HAS_BASICS, true);

                // hardcode check PLAYER_HAS_INSTRUMENTS
                if (gameC.GetValue(ConditionKeys.PLAYER_HAS_DSL) &&
                    gameC.GetValue(ConditionKeys.PLAYER_HAS_TELEPHONESOCKET) &&
                    gameC.GetValue(ConditionKeys.PLAYER_HAS_PERF))
                {
                    gameC.SetValue(ConditionKeys.PLAYER_HAS_INSTRUMENT, true);
                }

                // show gui
                GameEngine.instance.gui.ShowNotificator();
                GameEngine.instance.audioManager.PlayNotification();

            }
        }
    }
}
