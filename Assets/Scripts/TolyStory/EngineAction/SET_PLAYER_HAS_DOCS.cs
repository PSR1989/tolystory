﻿using static TolyStory.Engine.EngineKeys;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_HAS_DOCS : EAction
    {
        public override void doAction()
        {
            var gameC = GameEngine.instance.gameCondition;
            if (!gameC.GetValue(ConditionKeys.PLAYER_HAS_DOCS))
            {
                gameC.SetValue(ConditionKeys.PLAYER_HAS_DOCS, true);
                
                // show gui
                GameEngine.instance.gui.ShowNotificator();
                GameEngine.instance.audioManager.PlayNotification();
            }
        }
    }
}
