﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    /*
     * Квест в доме абонента. Установка розетки и настройка модема.
     * 
     */

    class Scene2SetTelephoneSocket : EAction
    {
        public GameObject sprite;
        public GameObject notebook;
        public string message;
        public override void doAction()
        {
            sprite.SetActive(true);
            notebook.SetActive(true);
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.SCENE2_INSIDE_SOCKET_READY, true);
            GameEngine.instance.gui.ShowNotificator(message);
            GameEngine.instance.audioManager.PlayNotification();

            Destroy(gameObject);
        }
    }
}
