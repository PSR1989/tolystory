﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_SCENE2_TALK_WITH_ABONENT : EAction
    {
        public GameObject trigger;
        public string message;
        private bool canRun = true;
        public override void doAction()
        {
            if (!canRun) return;

            canRun = false;
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.SCENE2_TALK_WITH_ABONENT, true);
            GameEngine.instance.gui.ShowNotificator(message);
            GameEngine.instance.audioManager.PlayNotification();
            trigger.SetActive(true);
        }
    }
}
