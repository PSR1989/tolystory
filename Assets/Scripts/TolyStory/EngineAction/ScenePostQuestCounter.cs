﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    /*
     * Квест на Почте. Оператор почты даёт задание починить телефон.
     * ГГ должен обойти 3 мусорки, что бы найти кабель.
     * 
     */

    class ScenePostQuestCounter : EAction
    {
        public override void doAction()
        {
            GameEngine.instance.scenePostQuestCounter++;

            if (GameEngine.instance.scenePostQuestCounter  >= 3)
            {
                GameEngine.instance.gui.ShowNotificator("Кажется ты нашел, кусочек провода.");
                GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.PLAYER_HAS_CABLE, true);
                GameEngine.instance.audioManager.PlayNotification();
            }
            else
            {
                GameEngine.instance.gui.ShowNotificator("Тут провода нет. Осмотри другую урну.");
                GameEngine.instance.audioManager.PlayUrna();
            }

            Destroy(gameObject);
        }
    }
}
