﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_SCENE1_POST_COMPLETE : EAction
    {
        public GameObject sprite;

        public override void doAction()
        {
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.SCENE1_QUEST_POST_COMPLETE, true);
            GameEngine.instance.gui.ShowNotificator("Вроде всё хорошо. Теперь поговорите с работником почты.");
            GameEngine.instance.audioManager.PlayNotification();
            sprite.SetActive(true);
            Destroy(gameObject);
        }
    }
}
