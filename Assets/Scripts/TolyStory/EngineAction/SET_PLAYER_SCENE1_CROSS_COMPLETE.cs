﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_SCENE1_CROSS_COMPLETE : EAction
    {

        public override void doAction()
        {
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.SCENE1_QUEST_CROSS_COMPLETE, true);
            GameEngine.instance.gui.ShowNotificator("Кроссировка завершена. Теперь садись в нивку и к абоненту.");
            GameEngine.instance.audioManager.PlayNotification();
            Destroy(gameObject);
        }
    }
}
