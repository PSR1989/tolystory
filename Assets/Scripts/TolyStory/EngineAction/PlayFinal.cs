﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class PlayFinal : EAction
    {
        public GameObject player;
        public GameObject npc;
        public GameObject sprite;

        public override void doAction()
        {
            player.SetActive(false);
            npc.SetActive(false);
            sprite.SetActive(true);

            // play final state
            GameEngine.instance.audioManager.PlayFinalMusic();
            GameEngine.instance.gameOver = true;
        }
    }
}
