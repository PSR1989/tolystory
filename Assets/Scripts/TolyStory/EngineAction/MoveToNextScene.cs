﻿using TolyStory.Items;
using TolyStory.Res;

namespace TolyStory.EngineAction
{
    public class MoveToNextScene : EAction
    {
        public MyScene parentScene;
        public MyScene nextScene;
        public override void doAction()
        {
            GameEngine.instance.player.SetNextXPosAndDirection(2.5f, true);
            GameEngine.instance.StartNewState(Values.STATE_TRANSITION);
            GameEngine.instance.audioManager.PlayDoorSound(DoorSound.Car);

            // add listener
            GameEngine.instance.gui.uiFader.OnFadeIn.AddListener(OnFaderFadeIn);
        }

        private void OnFaderFadeIn()
        {
            parentScene.Deactivate();
            nextScene.Activate();

            // remove listener
            GameEngine.instance.gui.uiFader.OnFadeIn.RemoveListener(OnFaderFadeIn);

        }
    }
}
