﻿using static TolyStory.Engine.EngineKeys;

namespace TolyStory.EngineAction
{
    class SET_PLAYER_HAS_TELEPHONESOCKET : EAction
    {
        public override void doAction()
        {
            var gameC = GameEngine.instance.gameCondition;

            if (!gameC.GetValue(ConditionKeys.PLAYER_HAS_TELEPHONESOCKET))
            {
                // magic here
                gameC.SetValue(ConditionKeys.PLAYER_HAS_TELEPHONESOCKET, true);
                GameEngine.instance.audioManager.PlayNotification();
                // hardcode
                if (gameC.GetValue(ConditionKeys.PLAYER_HAS_BASICS) &&
                    gameC.GetValue(ConditionKeys.PLAYER_HAS_DSL) &&
                    gameC.GetValue(ConditionKeys.PLAYER_HAS_PERF))
                {
                    gameC.SetValue(ConditionKeys.PLAYER_HAS_INSTRUMENT, true);
                }

                // show gui
                GameEngine.instance.gui.ShowNotificator("Ты нашел телефонную розетку RJ-11");
            }
        }
    }
}
