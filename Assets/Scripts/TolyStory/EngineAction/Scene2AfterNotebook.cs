﻿using TolyStory.Items;
using UnityEngine;

namespace TolyStory.EngineAction
{
    public class Scene2AfterNotebook : EAction
    {
        public DialogSequence dialog;
        public GameObject doorTriger;
        public GameObject doorTriggerSprite;

        private bool canRun;
        public override void doAction()
        {
            if (canRun) return;

            canRun = false;
            doorTriger.SetActive(true);
            doorTriggerSprite.SetActive(true);
            GameEngine.instance.StartDialogState(dialog);
            GameEngine.instance.audioManager.PlayNPCSOund(Res.NPCSound.a);
        }
    }
}
