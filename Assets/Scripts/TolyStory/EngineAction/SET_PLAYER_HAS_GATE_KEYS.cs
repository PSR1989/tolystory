﻿using UnityEngine;

namespace TolyStory.EngineAction
{
    public class SET_PLAYER_HAS_GATE_KEYS : EAction
    {
        private bool canRun = true;

        public override void doAction()
        {
            if (!canRun)
            {
                return;
            }
            canRun = false;
            GameEngine.instance.audioManager.PlayNotification();
            GameEngine.instance.gameCondition.SetValue(Engine.EngineKeys.ConditionKeys.PLAYER_HAS_GATE_KEYS, true);
            GameEngine.instance.gui.ShowNotificator("Ты получил ключ. Теперь можно открыть ворота.");
        }
    }
}
