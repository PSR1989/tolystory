﻿using UnityEngine;
using TolyStory.Controllers;

namespace TolyStory.Managers
{
    public class GameLoop: MonoBehaviour
    {
        public InputManager inputManager;
        public PlayerController player;

        void Update()
        {
            // reset input manager
            inputManager.action = false;
        }
    }
}
