﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TolyStory.Res;

public class AudioM : MonoBehaviour
{

    private AudioSource m_AudioSource;

    public AudioClip simpleDoor;
    public AudioClip stationDoor;
    public AudioClip carDoor;
    public AudioClip withOutDoor;
    public AudioClip click;
    public AudioClip monkeyA;
    public AudioClip monkeyB;
    public AudioClip monkeyC;
    public AudioClip monkeyD;
    public AudioClip notification;
    public AudioClip urna;

    public AudioClip finalMusic;
    public AudioSource musicPlayer;



    // Start is called before the first frame update
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    public void PlayUrna()
    {
        m_AudioSource.PlayOneShot(urna);
    }

    public void PlayClick()
    {
        m_AudioSource.PlayOneShot(click);
    }

    public void PlayNotification()
    {
        m_AudioSource.PlayOneShot(notification);
    }

    public void PlayNPCSOund(NPCSound npc)
    {
        if (npc == NPCSound.a)
        {
            m_AudioSource.PlayOneShot(monkeyA);
        }
        else if (npc == NPCSound.b)
        {
            m_AudioSource.PlayOneShot(monkeyB);
        }
        else if (npc == NPCSound.c)
        {
            m_AudioSource.PlayOneShot(monkeyC);
        }
        else if (npc == NPCSound.d)
        {
            m_AudioSource.PlayOneShot(monkeyD);
        }
    }

    public void PlayDoorSound(DoorSound ds)
    {
        if (ds == DoorSound.Simple)
        {
            m_AudioSource.PlayOneShot(simpleDoor);
        }
        else if (ds == DoorSound.Station)
        {
            m_AudioSource.PlayOneShot(stationDoor);
        }
        else if (ds == DoorSound.Car)
        {
            m_AudioSource.PlayOneShot(carDoor);
        }
        else if (ds == DoorSound.WithOut)
        {
            m_AudioSource.PlayOneShot(withOutDoor);
        }
    }

    public void PlayFinalMusic()
    {
        musicPlayer.Stop();
        musicPlayer.clip = finalMusic;
        musicPlayer.loop = false;
        musicPlayer.volume = 0.9f;
        musicPlayer.Play();
    }

}
