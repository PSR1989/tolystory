﻿using TolyStory.Items;
using TolyStory.Res;
using TolyStory.UI;
using UnityEngine;
using UnityEngine.UI;

namespace TolyStory.Managers
{
    public class GUIManager : MonoBehaviour
    {
        public UIDialogSystem uiDialogSystem;
        public UINotificator uiNotificator;
        public UIFader uiFader;
        public UIJournal uiJournal;
        public UIInventory uiInventory;
        public GameObject startMenu;
        public GameObject uiInput;
        public GameObject uiFinal;
        public Text finalText;
        public GameObject finlaBtn;
        public GameObject guiList;

        // unity
        public GameObject root;
        public Text actionButtonText;
        public GameObject specialBtn;
        public Text spectialBtnText;

        // ui values
        public bool isJournalShown = false;
        private byte actionButtonStatusCode = 0; // 0 - Journal, 1 - openDoor, 2 - talk, 4 - other

        public void StartDialog()
        {
            uiDialogSystem.StartDialog();
        }
        public void StopDialog()
        {
            uiDialogSystem.StopDialog();
        }
        public void ShowDialog(Dialog d)
        {
            uiDialogSystem.Show(d);
        }

        public void SetActionButtonNameToNextDialog()
        {
            actionButtonStatusCode = 4;
            actionButtonText.text = Values.GUI_ACTION_NEXT;
        }
        public void SetActionButtonNameToCloseDialog()
        {
            actionButtonStatusCode = 4;
            actionButtonText.text = Values.GUI_ACTION_CLOSE;
        }
        public void SetActionButtonNameToOpenDoor()
        {
            if (actionButtonStatusCode != 2)
            {
                actionButtonText.text = Values.GUI_ACTION_OPEN;
                actionButtonStatusCode = 2;
            }
        }
        public void SetActionButtonNameToOpenJournal()
        {
            if (actionButtonStatusCode != 0)
            {
                actionButtonText.text = Values.GUI_ACTION_JOURNAL;
                actionButtonStatusCode = 0;
            }
        }
        public void SetActionButtonNameToStartDialog()
        {
            if (actionButtonStatusCode != 1)
            {
                actionButtonText.text = Values.GUI_ACTION_TALK;
                actionButtonStatusCode = 1;
            }
        }

        public void ShowNotificator()
        {
            uiNotificator.Notify();
        }

        public void ShowNotificator(string message)
        {
            uiNotificator.Notify(message);
        }

        public void FadeEffect()
        {
            uiFader.Fade();
        }

        public void ShowJournal()
        {
            // update state
            uiJournal.Show();
            uiInventory._Update();
            isJournalShown = true;
        }

        public void HideJournal()
        {
            uiJournal.Hide();
            isJournalShown = false;
        }

        // special btn
        public void ShowSpecialBtn(string name)
        {
            if (!specialBtn.activeInHierarchy)
            {
                spectialBtnText.text = name;
                specialBtn.SetActive(true);
            }
        }

        public void HideSpecialBtn()
        {
            specialBtn.SetActive(false);
        }

        public void HideMenu()
        {
            startMenu.SetActive(false);
        }

        public void HideInput()
        {
            uiInput.SetActive(false);
        }

        public void ShowFinalBg()
        {
            uiFinal.SetActive(true);
        }

        public void SetFinalText(string txt)
        {
            finalText.text = txt;
        }

        public void ShowFinalBtn()
        {
            finlaBtn.SetActive(true);
        }
    }
}
