﻿using UnityEngine;

namespace TolyStory.Managers
{
    public class InputManager : MonoBehaviour
    {
        public float dx;
        public bool action;
        public bool special;

        public void MoveRight()
        {
            dx = 1;
        }

        public void MoveLeft()
        {
            dx = -1;
        }

        public void ResetMovement()
        {
            dx = 0;
        }

        public void SetAction(bool val)
        {
            action = val;
        }

        public void SetSpecial(bool val)
        {
            special = val;
        }
    }
}
