﻿using System.Collections.Generic;
using UnityEngine;
using TolyStory.Items;
using TolyStory.States;
using TolyStory.Managers;
using TolyStory.Res;
using TolyStory.Controllers;
using TolyStory.Engine;
using TolyStory.EngineAction;

public class GameEngine : MonoBehaviour
{
    public static GameEngine instance; // singletone
    public InputManager input;
    public AudioM audioManager;
    public GUIManager gui;
    public PlayerController player;
    public Sprites sprites;

    // game engine variables
    public bool isLastDialog = false;
    public bool doPreUpdate = true;

    private Dictionary<int, State> states; // game states storage
    public GameCondition gameCondition;

    private State curState; // current running state
    public EAction curAction;

    public DoorSound curDoorSound;

    //TODO delet for debug
    [Header("Пропуск стартового диалога")]
    public bool skipStartDialog = false;
    public DialogSequence startDialog;

    public CameraController cameraControler;

    //for quests
    // scene 1 post
    public int scenePostQuestCounter = 0;
    public int sceneStationOutdorCounter = 0;

    public bool gameOver = false; // set in PlayFinal EAction


    // unity
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        states = new Dictionary<int, State>();

        states.Add(Values.STATE_COMMON, new CommonState(this, player));
        states.Add(Values.STATE_DIALOG, new DialogState(this));
        states.Add(Values.STATE_START, new StartGame(this));
        states.Add(Values.STATE_TRANSITION, new Transition(this));
        states.Add(Values.STATE_JOURNAL, new JournalState(this));
        states.Add(Values.STATE_FINAL, new FinalState(this));

        // load or create
        gameCondition = new GameCondition();
    }

    private void Start()
    {
        // load game and set state and game variables
        if (skipStartDialog)
        {
            curState = states[Values.STATE_COMMON];
        }
        else
        {
            curState = states[Values.STATE_DIALOG];
            var dialogState = curState as DialogState;
            dialogState.SetDialogSequence(startDialog);
        }

    }
    void Update()
    {
        // main loop
        if (doPreUpdate)
        {
            curState.PreUpdate();
            doPreUpdate = false;
        }
        else{
            curState.Update(Time.deltaTime);
        }

        // udpate gui
        curState.UpdateGUI();
    }

    private void LateUpdate()
    {
        // reset input manager
        input.action = false;
        input.special = false;
    }

    public void StartNewState(int state)
    {
        // complete post update
        curState.PostUpdate();

        // set trigger to preupdate state
        doPreUpdate = true;

        curState = states[state];
    }

    public void HideMainMenu()
    {
        gui.HideMenu();
        audioManager.PlayClick();
    }

    public void StartDialogState(DialogSequence ds)
    {
        StartNewState(Values.STATE_DIALOG);
        var dialogState = curState as DialogState;
        dialogState.SetDialogSequence(ds);
    }
      
    public void RunCallback()
    {
        if (curAction != null)
        {
            curAction.doAction();
            curAction = null;
        }
    }

    public void StartFinalState()
    {
        curState.PostUpdate();
        doPreUpdate = true;
        curState = new FinalState(this);
    }

    public void RunCallback(string message)
    {
        if (curAction != null)
        {
            curAction.doAction(message);
            curAction = null;
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void HideGuiList()
    {
        gui.guiList.SetActive(false);
        audioManager.PlayClick();
    }

    public void HideJournal()
    {
        gui.HideJournal();
        audioManager.PlayClick();
    }
}
